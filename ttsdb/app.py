#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

import pymysql; pymysql.install_as_MySQLdb()

import flask as _f

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_security import (
	Security, SQLAlchemyUserDatastore
)

tts_blueprint = _f.Blueprint('ttsdb', __name__)

# ========================================

def init_app(app):
	db = SQLAlchemy(app)
	
	from ttsdb.database import init_db
	init_db(app, db)

	# Setup Flask-Security
	import ttsdb.models as _m
	user_datastore = SQLAlchemyUserDatastore(db, _m.User, _m.Role)
	security = Security(app, datastore=user_datastore)

	from ttsdb import routes
	routes.register(tts_blueprint)

	from ttsdb import api_v1
	api_v1.register(app, db, tts_blueprint)

	app.register_blueprint(tts_blueprint)

	migrate = Migrate(app, db)
