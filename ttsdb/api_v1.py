#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

import datetime as _datetime
import dateutil.parser as _dateutil_parser

from flask import make_response, request

from flask_security import (
	login_required,
	auth_required,
	http_auth_required,
	auth_token_required,
	current_user
)

from flask_restful import Api, Resource
from flask_cors import CORS

from marshmallow import Schema, fields, ValidationError, pre_load


from ttsdb import models as _m


class DBResource(Resource):
	def __init__(self, db, *args, **kwargs):
		super(DBResource, self).__init__(*args, **kwargs)
		self._db = db


class ProtectedResource(DBResource):
	method_decorators = [
		# require any one of these auth methods
		auth_required('session', 'token', 'basic')
	]


class TimesheetItemCategorySchema(Schema):
	id = fields.Integer()
	name = fields.String()

class OrderedTimesheetItemCategorySchema(Schema):
	order = fields.Integer()
	category = fields.Nested(
		TimesheetItemCategorySchema(),
		many=False,
		required=True
	)

class UserSchema(Schema):
	id = fields.Integer()
	email = fields.String()
	categories = fields.Nested(
		OrderedTimesheetItemCategorySchema(),
		many=True,
		required=False
	)



class UserResource(ProtectedResource):
	# R
	def get(self):
		return UserSchema().dump(current_user)

class UserCategoriesResource(ProtectedResource):
	# R
	def get(self):
		return OrderedTimesheetItemCategorySchema().dump(
			current_user.categories,
			many=True
		)

	# U
	def put(self):
		assert request.json, "No request data given"
		assert isinstance(request.json.get('categories'), list), "Request data format invalid"
		for uc in current_user.categories:
			self._db.session.delete(uc)
		for i, cat in enumerate(request.json['categories']):
			catm = _m.TimesheetItemCategory.query.filter_by(name=cat).first()
			if not catm:
				catm = _m.TimesheetItemCategory(name=cat)
				self._db.session.add(catm)
			uc = _m.TimesheetItemCategory_User(user=current_user, category=catm, order=i)
			self._db.session.add(uc)
		self._db.session.commit()
		return self.get()


class TimesheetItemSchema(Schema):
	id = fields.Integer()
	timesheet_id = fields.Integer()
	description = fields.String()
	time_started = fields.DateTime()
	categories = fields.Nested(
		TimesheetItemCategorySchema(),
		many=True,
		required=False,
		only='name'
	)
	duration = fields.TimeDelta()
	duration_per_category = fields.TimeDelta()


class Timesheet_Simple_Schema(Schema):
	id = fields.Integer()
	name = fields.String()
	created = fields.DateTime()
	updated = fields.DateTime()
	min_item_time = fields.DateTime()
	max_item_time = fields.DateTime()

class Timesheet_WithItems_Schema(Timesheet_Simple_Schema):
	items = fields.Nested(TimesheetItemSchema, many=True)

class Timesheet_Full_Schema(Timesheet_WithItems_Schema):
	user = fields.Nested(UserSchema)


class TimesheetsResource(ProtectedResource):
	# R(many)
	def get(self):
		result = _m.Timesheet.query \
			.filter_by(
				user_id = current_user.id
			) \
			.all()
		if request.args.get('detail') == 'items':
			return Timesheet_WithItems_Schema(many=True).dump(result)
		else:
			return Timesheet_Simple_Schema(many=True).dump(result)

def calculate_minmax(timesheet):
	items = timesheet.items

	if items:
		times = [
			_dateutil_parser.parse(i.time_started)
				if isinstance(i.time_started, str) else i.time_started
			for i in items
		]

		min_item_time = min(times)
		max_item_time = max(times)
		timesheet.min_item_time = min_item_time
		timesheet.max_item_time = max_item_time


class TimesheetResource(ProtectedResource):
	# R
	def get(self, timesheet_id):
		result = _m.Timesheet.query \
			.filter_by(
				id = timesheet_id,
				user_id = current_user.id
			) \
			.first()
		# calculate the item durations
		for i, item in enumerate(result.items[:-1]):
			item.duration = result.items[i+1].time_started - item.time_started
			item.duration_per_category = (item.duration / len(item.categories)) if len(item.categories) > 0 else 0
		result.items[-1].duration = _datetime.timedelta(0)
		result.items[-1].duration_per_category = _datetime.timedelta(0)
		return Timesheet_Full_Schema().dump(result)

	# C
	def put(self):
		sheet = _m.Timesheet(**request.json)
		sheet.user_id = current_user.id
		self._db.session.add(sheet)
		calculate_minmax(sheet)
		self._db.session.commit()
		return Timesheet_Simple_Schema().dump(sheet)

	# U
	def patch(self, timesheet_id):
		assert request.json, "No request data given"

		sheet = _m.Timesheet.query.get(timesheet_id)
		assert sheet is not None, "Invalid sheet ID"
		assert current_user.id == sheet.user.id, "Invalid sheet ID"

		input_sheet = Timesheet_Simple_Schema().load(request.json)
		assert not input_sheet.errors, "Request data is not valid: %r" % (input_sheet.errors,)

		sheet.updated = _datetime.datetime.utcnow()
		sheet.name = input_sheet.data['name']

		self._db.session.add(sheet)

		calculate_minmax(sheet)
		self._db.session.add(sheet)
		self._db.session.commit()

		return Timesheet_Simple_Schema().dump(sheet)


class TimesheetItemResource(ProtectedResource):

	def _get_or_create_categories(self, categories):
		category_models = {}
		for category in categories:
			model = _m.TimesheetItemCategory.query.filter_by(name=category).one_or_none()
			if not model:
				model = _m.TimesheetItemCategory(name=category)
				self._db.session.add(model)
			category_models[category] = model
		return category_models

	def _get_input_item_categories(self, item=None):
		assert request.json, "No request data given"
		input_categories = request.json.pop(
			'categories',
			[c.name for c in (item.categories or [])] if item else []
		) or []
		input_item = TimesheetItemSchema().load(request.json)
		assert not input_item.errors, "Request data is not valid: %r" % (input_item.errors,)

		category_mapping = self._get_or_create_categories(input_categories)
		return input_item, category_mapping

	def _update_item_categories(self, item, category_mapping):
		while len(item.categories):
			item.categories.pop()
		for category, model in category_mapping.items():
			item.categories.append(model)

	def _get_sheet_for_date(self, time_started):
		# TODO having date field on Timesheet model would
		# be better than using the string formatted name
		sheet_name = time_started.strftime('%a %d %b %Y')
		sheet = _m.Timesheet.query.filter_by(
			user_id = current_user.id,
			name = time_started.strftime('%a %d %b %Y')
		).one_or_none()
		if not sheet:
			sheet = _m.Timesheet()
			sheet.name = sheet_name
			sheet.user_id = current_user.id
			sheet.created = _datetime.datetime.utcnow()
			sheet.updated = _datetime.datetime.utcnow()
			self._db.session.add(sheet)
		return sheet

	# C
	def put(self):
		input_item, category_mapping = self._get_input_item_categories()

		item = _m.TimesheetItem(**request.json)
		assert item.time_started, "Item has no start time"

		self._update_item_categories(item, category_mapping)

		# ensure we do not create a duplicate
		sheet = self._get_sheet_for_date(input_item.data['time_started'])
		def _category_set(m):
			return set([c.name for c in m.categories])

		existing_item = [
			i for i in sheet.items
				if i.time_started == input_item.data['time_started'] \
				and _category_set(i) == _category_set(item)
		]
		
		# return the existing match if there is one
		if existing_item:
			return TimesheetItemSchema().dump(existing_item[0])

		# if the timesheet_id was not specified, attach the
		# item to the correct sheet
		if not item.timesheet_id:
			# find / create the timesheet parent
			self._db.session.commit()
			item.timesheet_id = sheet.id
			sheet.updated = _datetime.datetime.utcnow()

		self._db.session.add(item)

		calculate_minmax(sheet)
		self._db.session.add(sheet)
		self._db.session.commit()

		return TimesheetItemSchema().dump(item)

	
	# R
	def get(self, timesheet_item_id=None):
		assert timesheet_item_id is not None, "Invalid item ID"

		item = None
		if timesheet_item_id == 'latest':
			# ??
			item = _m.TimesheetItem.query.join(_m.Timesheet).join(_m.User).filter(
				_m.User.id == current_user.id
			).order_by(_m.TimesheetItem.time_started.desc()).first()
		else:
			item = _m.TimesheetItem.query.get(timesheet_item_id)

		assert item is not None, "Invalid item ID"
		assert current_user.id == item.timesheet.user.id, "Invalid item ID"
		return TimesheetItemSchema().dump(item)

	# U
	def patch(self, timesheet_item_id):
		item = _m.TimesheetItem.query.get(timesheet_item_id)
		assert item is not None, "Invalid item ID"
		assert current_user.id == item.timesheet.user.id, "Invalid item ID"

		input_item, category_mapping = self._get_input_item_categories(item=item)

		item.time_started = input_item.data.get('time_started', item.time_started)
		item.description = input_item.data.get('description', item.description)
		self._update_item_categories(item, category_mapping)

		self._db.session.add(item)

		calculate_minmax(item.timesheet)
		item.timesheet.updated = _datetime.datetime.utcnow()
		self._db.session.add(item.timesheet)
		self._db.session.commit()

		return TimesheetItemSchema().dump(item)

class LatestTimesheetItemResource(TimesheetItemResource):
	# R
	def get(self):
		return super(LatestTimesheetItemResource, self).get('latest')

@login_required
def token():
	return current_user.get_auth_token()


def register(app, db, blueprint):
	api = Api(app=app)

	blueprint.route('/api/v1/token')(token)

	api.add_resource(
		UserResource, '/api/v1/user',
		resource_class_args=[db]
	)
	api.add_resource(
		UserCategoriesResource, '/api/v1/user/categories',
		resource_class_args=[db]
	)

	api.add_resource(
		TimesheetsResource, '/api/v1/timesheets', # accepts ?detail=items
		resource_class_args=[db]
	)
	api.add_resource(
		TimesheetResource,
			'/api/v1/timesheet',
			'/api/v1/timesheet/<int:timesheet_id>',
		resource_class_args=[db]
	)

	api.add_resource(
		TimesheetItemResource,
			'/api/v1/timesheet_item',
			'/api/v1/timesheet_item/<int:timesheet_item_id>',
		resource_class_args=[db]
	)

	api.add_resource(
		LatestTimesheetItemResource,
			'/api/v1/timesheet_item/latest',
		resource_class_args=[db]
	)

	cors = CORS(app, resources={
		r'/api/*': { 'origins': '*' }
	})