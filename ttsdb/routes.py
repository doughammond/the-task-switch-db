#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 


import os as _os

from flask import (
	send_from_directory,
	current_app,
	abort
)

from flask_security import (
	login_required
)

@login_required
def app(path):
	sp = current_app.config['APP_BUILD']
	fp = _os.path.join(sp, path)
	if _os.path.exists(fp):
		return send_from_directory(sp, path)
	else:
		# all non-real paths point to the index, since the app
		# uses react-router for changing view in the front-end
		return send_from_directory(sp, 'index.html')


def register(blueprint):
	blueprint.route('/app/', defaults={'path': 'index.html'})(app)
	blueprint.route('/app/<path:path>')(app)
