#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

from flask_sqlalchemy import orm
from flask_sqlalchemy import sqlalchemy as _sa


from flask_security import (
	UserMixin, RoleMixin
)

from ttsdb.database import Base


roles_users = _sa.Table(
	'roles_users',
	Base.metadata,
	_sa.Column('user_id', _sa.Integer(), _sa.ForeignKey('user.id')),
	_sa.Column('role_id', _sa.Integer(), _sa.ForeignKey('role.id'))
)

class Role(Base, RoleMixin):
	__tablename__ = 'role'

	id = _sa.Column(_sa.Integer(), primary_key=True)
	name = _sa.Column(_sa.String(80), unique=True)
	description = _sa.Column(_sa.String(255))

# allow many-to-many between user and categories, for the user to configure their client
class TimesheetItemCategory_User(Base):
	__tablename__ = 'timesheet_item_category_user'

	id = _sa.Column(_sa.Integer(), primary_key=True)
	timesheet_item_category_id = _sa.Column(_sa.Integer(), _sa.ForeignKey('timesheet_item_category.id'))
	user_id = _sa.Column(_sa.Integer(), _sa.ForeignKey('user.id'))
	order = _sa.Column(_sa.Integer())



class User(Base, UserMixin):
	__tablename__ = 'user'

	id = _sa.Column(_sa.Integer(), primary_key=True)
	email = _sa.Column(_sa.String(255), unique=True)
	password = _sa.Column(_sa.String(255))
	active = _sa.Column(_sa.Boolean())
	confirmed_at = _sa.Column(_sa.DateTime())
	roles = orm.relationship(
		'Role', secondary=roles_users,
		backref=orm.backref('users', lazy='dynamic')
	)

	categories = orm.relationship(
		'TimesheetItemCategory_User',
		backref=orm.backref('user'),
		primaryjoin=id == TimesheetItemCategory_User.user_id
	)


class Timesheet(Base):
	__tablename__ = 'timesheet'

	id = _sa.Column(_sa.Integer(), primary_key=True)
	user_id = _sa.Column(_sa.Integer(), _sa.ForeignKey('user.id'))
	name = _sa.Column(_sa.String(255))
	created = _sa.Column(_sa.DateTime())
	updated = _sa.Column(_sa.DateTime())
	min_item_time = _sa.Column(_sa.DateTime())
	max_item_time = _sa.Column(_sa.DateTime())

	user =  orm.relationship('User')
	items = orm.relationship('TimesheetItem')


class TimesheetItemCategory(Base):
	__tablename__ ='timesheet_item_category'

	id = _sa.Column(_sa.Integer(), primary_key=True)
	name = _sa.Column(_sa.String(255), unique=True)

	users = orm.relationship(
		'TimesheetItemCategory_User',
		backref=orm.backref('category'),
		primaryjoin=id == TimesheetItemCategory_User.timesheet_item_category_id
	)

# many-to-many between timesheet items and categories
timesheet_item_category_timesheet_item = _sa.Table(
	'timesheet_item_category_timesheet_item',
	Base.metadata,
	_sa.Column('timesheet_item_category_id', _sa.Integer(), _sa.ForeignKey('timesheet_item_category.id')),
	_sa.Column('timesheet_item_id',          _sa.Integer(), _sa.ForeignKey('timesheet_item.id'))
)



class TimesheetItem(Base):
	__tablename__ = 'timesheet_item'

	id = _sa.Column(_sa.Integer(), primary_key=True)
	timesheet_id = _sa.Column(_sa.Integer(), _sa.ForeignKey('timesheet.id'))
	description = _sa.Column(_sa.String(255))
	time_started = _sa.Column(_sa.DateTime())
	
	categories = orm.relationship(
		'TimesheetItemCategory', secondary=timesheet_item_category_timesheet_item,
		backref=orm.backref('timesheet_items', lazy='dynamic')
	)

	timesheet = orm.relationship('Timesheet')
